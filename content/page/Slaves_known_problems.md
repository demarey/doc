---
title: "Slaves known problems"
---
Mac
===

Mac OSX 10.9
------------

### Screen saver

The screen saver is activated and is taking CPU and memory :

<code>\$ ps ax \| grep -i screensaver \| grep -v grep

` 612   ??  Us   11479:59.42 /System/Library/Frameworks/ScreenSaver.framework/Versions/A/Resources/ScreenSaverEngine.app/Contents/MacOS/ScreenSaverEngine -loginWindow`

</code>

You can desctivate it this way in command line :

`$ sudo su -l "ci" -c "defaults -currentHost write com.apple.screensaver idleTime 0"`

You can check it this way :

`$ defaults -currentHost read com.apple.screensaver idleTime`

You have to reboot your instance :

`$ sudo shutdown -r now`

### No root user

Mac OSX has no root user, but ci user has been granted.

`$ sudo my-command`\
`Password: **ci password**`

### NTP

Mac OSX ntp is not always up to date. It is advised to use INRIA's one

`$ systemsetup -setnetworktimeserver ntp.inria.fr`

Linux
=====

Ubuntu 16.04
------------

### No root user

Ubuntu 16.04 has no root user for security reasons, but ci user has been
granted. You can do

<code>\$ sudo my-command

Password: \*\*ci password\*\*</code>

Windows
=======

Windows 7 x 64
--------------

### No build directory

Win7x64 has no c:\\builds directory
